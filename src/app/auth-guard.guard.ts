import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const id = route.paramMap.get("id");
      const token = localStorage.getItem("token");
      if ( token && token === "authenticated" && (+id === 1) ){
        return true;
      }
      console.log("you do not have a valid token.")
      return false;;
  }
  
}
