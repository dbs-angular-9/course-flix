import {HttpRequest, HttpInterceptor, HttpHandler, HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private router: Router){}
    intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>>{
        const userToken = localStorage.getItem("user");
        if (!userToken){
            this.router.navigate(["/login"]);
        } else {
        
           req =  req.clone({
                setHeaders:{
                    'Authorization': `Bearer ${userToken}` 
                }
            })
            console.log("request object....")
            console.log(req);
        }
        return next.handle(req);
    }
}
