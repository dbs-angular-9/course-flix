import { Directive, Input, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[formatPhone]'
})
export class PhoneFormatterDirective {

  constructor(private element:ElementRef) { }

  phoneNumberFormat = function(countryCode){
    console.log("The country code is "+ countryCode)
    if(countryCode === 'IN'){
      return '+ 91';
    } else {
      return '+ 41'
    }
  }

  @Input('formatPhone') countryCode;

  @HostListener('focus') onFocus(){
    console.log("Inside the focu method")
  }

  @HostListener("blur")
  onBlur(){
    const inputValue = this.element.nativeElement.value;
    this.element.nativeElement.value = this.phoneNumberFormat(this.countryCode)+ ' ( ' + inputValue +' ) ';
  }

}
