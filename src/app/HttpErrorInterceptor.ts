import {HttpRequest, HttpInterceptor, HttpHandler, HttpEvent, HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

export class HttpErrorInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>>{
        console.log("Inside the Interceptor ");
        console.log("Request");
        console.log(req);
        return next.handle(req)
                    .pipe(
                        retry(3),
                        catchError((error:HttpErrorResponse)=>{
                            console.log(error); 
                            let message = " "; 
                            console.log(typeof error);
                            if (error.error instanceof ErrorEvent){
                                console.log('This is a client side error')
                                message = `Client side error ${error.message}`;
                            } else {
                                console.log("This is a server side error")
                                message  = `server side error ${error.message}`;
                            }
                            return throwError(message);
                        }
                    ))}
}