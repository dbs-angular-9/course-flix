import { Injectable } from '@angular/core';
import { Course } from './Course';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {retry, catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  courses: Course[];
  API_URI="https://my-json-server.typicode.com/prashdeep/courseflix/courses";

  constructor(private httpClient: HttpClient) { }

  fetchAllCourses = ():Observable<Course[]> => this.httpClient.get<Course[]>(this.API_URI)
          

  fetchCourseById = (id:number): Observable<Course> => this.httpClient.get<Course>(this.API_URI+"/"+id);
  
  submitCourse = (course:Course):Observable<Course> => this.httpClient.post<Course>(this.API_URI, course);

  deleteCourse = (id:number):Observable<void> => this.httpClient.delete<void>(this.API_URI+"/"+id);

  handleError(error){
    console.log("Error handled ", error);
    console.log(typeof error)
  }


}