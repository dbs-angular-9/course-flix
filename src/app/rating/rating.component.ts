import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit{

  @Input("rating") rating:number;
  fraction:boolean;
  ratingArray:number[];

  ngOnInit(){
    const wholeNumber = Math.floor(this.rating);
    this.ratingArray = new Array(wholeNumber);
    this.fraction = (this.rating - wholeNumber) !== 0;
  }

}
