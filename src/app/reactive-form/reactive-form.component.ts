import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { CourseService } from '../course.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Course } from '../Course';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit, OnDestroy {

  constructor(private courseService:CourseService, private router:Router) { }

  ngOnInit(): void {
    console.log("Ng on init called from model driven form")
    console.log("Inside the init method" +this.heading);
  }



  ngOnDestroy():void{
    console.log("NgOnDestroy called for model driven form")
  }

  ngAfterViewInit(){
    console.log("Ng Afterview init is called for the reactive forms component");
    console.log("This is where I can access the template variable ", this.heading);
    this.heading.nativeElement.innerText = "Content Modified...."
  }

  @ViewChild("heading") heading: ElementRef;

  modelForm = new FormGroup({
    courseName: new FormControl('',[
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(16)
    ]),
    coursePrice: new FormControl('',[
      Validators.required
    ]),

    startDate: new FormControl('',[Validators.required]),

    offer: new FormControl('true',[])
  })

  get coursename(){
    return this.modelForm.get("courseName");
  }

  get courseprice(){
    return this.modelForm.get("coursePrice");
  }

  get startdate(){
    return this.modelForm.get("startDate");
  }

  get offer(){
    return this.modelForm.get("offer");
  }


  handleSubmit(){
    const course:Course = this.modelForm.value;
    course.name = this.modelForm.value.courseName;
    course.price = this.modelForm.value.coursePrice;
    course.id = 56;
    course.rating = 5;
    console.log(course);
    this.courseService.submitCourse(course);
    this.router.navigate(['courses']);
  }
}
