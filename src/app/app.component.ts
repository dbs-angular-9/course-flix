  import { Component } from '@angular/core';

  @Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
  })
  export class AppComponent {
    //data 
    title = 'Welcome to Angular 9 workshop Day-2';
    course=[];
    courses=[
      {
        "name":"Angular",
        "price":"15000",
        "rating": 4.567,
        "offer":false,
        "startDate":"2020-09-09"
      },
      {
        "name":"React",
        "price":"16000",
        "rating": 4.678,
        "offer":false,
        "startDate":"2020-11-11"
      },
      {
        "name":"Spring Boot",
        "price":"25000",
        "rating": 4.878,
        "offer":true,
        "startDate":"2020-10-10"
      }
    ]

    handleSelect(selectedCourse){
      console.log("Inside the parent container");
      console.log(selectedCourse.name);
    }
  }
