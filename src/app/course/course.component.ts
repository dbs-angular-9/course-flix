import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CourseService } from '../course.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent{

  constructor(private courseService:CourseService){}

  @Input("data") course;
  subscription:Subscription;

  @Output('selected') courseSelected = new EventEmitter();

  ngOnInit(){
    this.subscription = this.courseService.fetchAllCourses().subscribe();
  }

  selectCourse(event){
    //propogate the event to the parent component
    console.log("event clicked ", event);
    //this.courseSelected.emit(course);
  }

  delete(id){
    this.courseService.deleteCourse(id).subscribe(data => console.log(data));
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  
}
