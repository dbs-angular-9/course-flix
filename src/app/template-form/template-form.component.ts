import { Component, OnInit, OnDestroy } from '@angular/core';
import { CourseService } from '../course.service';
import { Course } from '../Course';
import { Router } from '@angular/router';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit, OnDestroy {

  constructor(private courseService:CourseService, private router:Router) { }

  ngOnInit(): void {
    console.log("Ng on init called from template driven form")
  }

  ngOnDestroy():void{
    console.log("NgOnDestroy called for template driven form")
  }

  handleSubmit({courseName, coursePrice, startDate,offer }){
    let course: Course = {id:0, name:"", price:"",rating:5, offer:false, startDate: "" }; 
    console.log(courseName)
    course.name = courseName;
    course.price = coursePrice;
    course.rating = 5;
    course.startDate = startDate;
    course.offer = offer;
    this.courseService.submitCourse(course).subscribe(data=> console.log(data));
    this.router.navigate(['courses'])
  }

}
