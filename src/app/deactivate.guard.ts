import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { CourseDetailsComponent } from './course-details/course-details.component';

@Injectable({
  providedIn: 'root'
})
export class DeactivateGuard implements CanDeactivate<CourseDetailsComponent> {
  canDeactivate(component: CourseDetailsComponent, 
                currentRoute: ActivatedRouteSnapshot, 
                currentState: RouterStateSnapshot, 
                nextState?: RouterStateSnapshot):boolean{
    return confirm("You really want to exit the page ?")
  }
  
}
