import { Route } from '@angular/router';
import { CoursesComponent } from './courses/courses.component';
import { CompOneComponent } from './comp-one/comp-one.component';
import { CompTwoComponent } from './comp-two/comp-two.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { AuthGuard } from './auth-guard.guard';
import { DeactivateGuard } from './deactivate.guard';
import { TemplateFormComponent } from './template-form/template-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { CreateCourseComponent } from './create-course/create-course.component';
import { CustomComponent } from './custom/custom.component';
import { LoginComponent } from './login/login.component';

export const routes:Route[]=[
    {
      path:'',
      redirectTo:'courses',
      pathMatch:'full'
    },
    {
      path:'courses',
      component: CoursesComponent
    },
    {
        path:'courses/:id',
        component:CourseDetailsComponent,
        canActivate:[AuthGuard],
        canDeactivate:[DeactivateGuard]
    },
    {
      path:'create-course',
      component:CreateCourseComponent,
      children:[
          {
            path:'',
            redirectTo:"template",
            pathMatch:'full'
          },
          {
            path:'template',
            component:TemplateFormComponent
          },
          {
              path:'reactive',
              component:ReactiveFormComponent
          }
          
      ]
    },
    {
      path:'directives',
      component:CustomComponent
    },
    {
      path:'login',
      component:LoginComponent
    },
    {
      path:'orders',
      loadChildren: () => import('./orders/orders.module').then(m => m.OrdersModule)
    },
    {
        path:'**',
        component:NotFoundComponent
    }
  ]