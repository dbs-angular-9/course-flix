import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { Course } from '../Course';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
})
export class CoursesComponent implements OnInit{
  title = 'Welcome to Angular 9 workshop Day-2';
   courses:Course[];
  constructor(private courseService:CourseService){}

  ngOnInit(){
    this.courseService
        .fetchAllCourses()
        .subscribe(data => this.courses = data);
  }

  handleSelect(selectedCourse: Course){
    console.log("Inside the parent container");
    console.log(selectedCourse.name);
  }
}
