export interface Course {

    id: number;
    name: string,
    price:string,
    rating: number,
    offer:boolean,
    startDate:string

}