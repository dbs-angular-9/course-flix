import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coursename'
})
export class NamePipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    let coursename = value;
    if ( args && args[0] === 'lower'){
      coursename = value.toLowerCase()
    } else {
      coursename = value.toUpperCase();
    }
    return coursename;
  }

}
