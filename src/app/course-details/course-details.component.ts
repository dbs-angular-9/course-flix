import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourseService } from '../course.service';
import { Course } from '../Course';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {

  course:Course;

  constructor(private activateRoute: ActivatedRoute, private courseService:CourseService) { }

  ngOnInit(): void {
    const courseId = this.activateRoute.snapshot.paramMap.get('id');
    console.log(`Inside the course details component with id ${courseId}`)
    this.courseService.fetchCourseById(+courseId).subscribe(data => this.course = data)
  }

}
