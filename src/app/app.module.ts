import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { CourseComponent } from './course/course.component';
import { CoursesComponent } from './courses/courses.component';
import { CompOneComponent } from './comp-one/comp-one.component';
import { CompTwoComponent } from './comp-two/comp-two.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { routes } from './routes';
import { RatingComponent } from './rating/rating.component';
import { TemplateFormComponent } from './template-form/template-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { CreateCourseComponent } from './create-course/create-course.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CustomComponent } from './custom/custom.component';
import { PhoneFormatterDirective } from './phone-formatter.directive';
import { OrdersModule } from './orders/orders.module';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { HttpErrorInterceptor } from './HttpErrorInterceptor';
import { LoginComponent } from './login/login.component';
import { AuthInterceptor } from './AuthInterceptor';
import { NamePipe } from './name.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CourseComponent,
    CoursesComponent,
    NavBarComponent,
    NotFoundComponent,
    CourseDetailsComponent,
    RatingComponent,
    TemplateFormComponent,
    ReactiveFormComponent,
    CreateCourseComponent,
    CustomComponent,
    PhoneFormatterDirective,
    LoginComponent,
    NamePipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    OrdersModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
